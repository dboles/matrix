#ifndef DJB_MATRIX_HPP
#define DJB_MATRIX_HPP

#include "ensure.hpp"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <concepts>
#include <functional>
#include <iostream>
#include <optional>
#include <span>
#include <stdexcept>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

namespace djb {

namespace matrix {

/// a class representing a rectangular array of numbers or other elements
template<typename T_Element>
class Matrix {
public:
	/// the type of elements
	using value_type = T_Element;

	/// the container used to represent elements
	using elements_type = std::vector<value_type>;

	/// the type of indices and lengths
	using size_type = typename elements_type::size_type;


	/** Construct a Matrix of given size, with default-initialised elements.
	  *
	  * @param height the number of rows
	  * @param width the number of columns
	  *
	  * @throw std::invalid_argument if @p height or @p width are not >= 1
	  */
	Matrix(size_type const height,
	       size_type const width)
	: Matrix{validated_size(height, width), false}
	{
	}

	/** Construct a Matrix of given size, with given elements.
	  *
	  * @param height the number of rows
	  * @param width the number of columns
	  * @param elements the initial elements
	  *
	  * @throw std::invalid_argument if @p height or @p width are not >= 1
	  * @throw std::length_error if @p elements is not @p height * @p width
	  */
	Matrix(size_type const height,
	       size_type const width ,
	       elements_type elements)
	: Matrix{validated_size(height, width), std::move(elements),
	         false}
	{
		ensure<std::length_error>(m_elements.size() == height * width,
		                          "elements.size() must be == height * width");
	}


	/// Get the number of rows.
	auto
	get_height() const noexcept -> size_type
	{
		return m_height;
	}

	/// Get the number of columns.
	auto
	get_width() const noexcept -> size_type
	{
		return m_width;
	}

	/// Get a zero matrix of the same dimension as this one.
	auto
	get_zero_matrix() const& noexcept -> Matrix
	{
		return Matrix{ Sizes{m_height, m_width} };
	}

	/// @copybrief Matrix::get_zero_matrix()
	auto
	get_zero_matrix() && noexcept -> Matrix
	{
		zero_this();
		return std::move(*this);
	}

	/// Get whether the Matrix is square.
	auto
	is_square() const noexcept -> bool
	{
		return m_height == m_width;
	}

	/** If this Matrix is_square(), get the identity Matrix for its dimensions.
	  *
	  * @throw std::domain_error if this Matrix is not square
	  */
	auto
	get_identity_matrix() const& -> Matrix
	{
		ensure_square();

		auto a = get_zero_matrix();
		a.iden_this();
		return a;
	}

	/// @copydoc Matrix::get_identity_matrix()
	auto
	get_identity_matrix() && -> Matrix
	{
		ensure_square();

		zero_this();
		iden_this();
		return std::move(*this);
	}

	/// Get whether this and another Matrix have the same get_height() and get_width().
	auto
	has_same_dimension(Matrix const& that) const noexcept -> bool
	{
		return m_height == that.m_height and
		       m_width  == that.m_width;
	}

	/** Get whether this Matrix is equal to another.
	  *
	  * Two Matrix instances are considered equal if their dimensions are
	  * equal and all elements at all corresponding positions compare equal.
	  *
	  * @see has_same_dimension(), equal_below_epsilon(), operator!=()
	  *
	  * @param that the other Matrix
	  */
	auto
	operator==(Matrix const& that) const noexcept -> bool
	{
		if (this == &that) return true;

		return has_same_dimension(that) and
		       m_elements == that.m_elements;
	}

	/** Get whether this matrix is equal to another with a given tolerance.
	  *
	  * This is like operator==(), but the comparison for equality is fuzzy:
	  * if the absolute difference between any two corresponding elements
	  * is less than @p epsilon, those elements are considered to be equal.
	  *
	  * @see has_same_dimension(), operator==()
	  *
	  * @param that the other Matrix
	  * @param epsilon the minimum difference between elements required for
	  *                them to be considered as not equal to each other.
	  */
	auto
	equal_below_epsilon(Matrix const& that, value_type const epsilon) const noexcept -> bool
	{
		if (this == &that) return true;
		if (epsilon == 0 ) return *this == that;

		return has_same_dimension(that) and
		       std::equal( pbegin(), pend(), that.pbegin(),
		                   [=](value_type const a, value_type const b)
		                   { return std::abs(a - b) < epsilon; } );
	}


	// C++23: Replace these with multidimensional operator[]
	/** Get the element at a specified position.
	  *
	  * Note that this returns a reference to the element. From this, you
	  * can use pointer arithmetic to move from that to other elements: the
	  * Matrix stores its elements as a single contiguous row-major array.
	  *
	  * This does not bounds-check @p row or @p col; to get that, use at().
	  *
	  * @see at()
	  *
	  * @param row the row position of the element
	  * @param col the column position of the element
	  */
	auto
	operator()(size_type const row, size_type const col) const noexcept -> value_type const&
	{
		return m_elements[row * m_width + col];
	}

	/// @copydoc Matrix::operator()()
	auto
	operator()(size_type const row, size_type const col) noexcept -> value_type&
	{
		return const_cast<value_type&>( std::as_const(*this)(row, col) );
	}

	/** Get the element at a specified position, throwing if out-of-bounds.
	  *
	  * This method provides the same allowance for pointer arithmetic as
	  * operator()(), but of course, it cannot bounds-check that for you.
	  *
	  * @see operator()()
	  *
	  * @param row the row position of the element
	  * @param col the column position of the element
	  *
	  * @throw std::out_of_range if @p row or @p col exceed their bound
	  */
	auto
	at(size_type const row, size_type const col) const -> value_type const&
	{
		validate_row(row);
		validate_col(col);

		return (*this)(row, col);
	}

	/// @copydoc Matrix::at()
	auto
	at(size_type const row, size_type const col) -> value_type&
	{
		return const_cast<value_type&>( std::as_const(*this).at(row, col) );
	}


	/** Add another Matrix to this one.
	  *
	  * Both Matrix instances must have the same dimensions.
	  *
	  * @param that the other Matrix
	  *
	  * @throw std::domain_error if @p that has different dimensions
	  */
	auto
	operator+=(Matrix const& that) -> Matrix&
	{
		if (&that == this) {
			*this *= 2;
		} else {
			transform(std::plus<value_type>{}, that);
		}

		return *this;
	}

	/// Get the result of calling operator+=() on a copy of this Matrix.
	auto
	operator+(Matrix const& that) const& -> Matrix
	{
		return Matrix{*this} += that;
	}

	/// Get the result of calling operator+=() on this rvalue Matrix.
	auto
	operator+(Matrix const& that) && -> Matrix
	{
		return std::move(*this += that);
	}

	/// Get the result of calling operator+=() on this rvalue Matrix.
	auto
	operator+(Matrix&& that) && -> Matrix
	{
		return std::move(*this += that);
	}

	/// Get the result of calling operator+=() on @p that rvalue Matrix.
	auto
	operator+(Matrix&& that) const& -> Matrix
	{
		return std::move(that += *this);
	}


	/** Subtract another Matrix from this one.
	  *
	  * Both Matrix instances must have the same dimensions.
	  *
	  * @param that the other Matrix
	  *
	  * @throw std::domain_error if @p that has different dimensions
	  */
	auto
	operator-=(Matrix const& that) -> Matrix&
	{
		if (&that == this) {
			zero_this();
		} else {
			transform(std::minus<value_type>{}, that);
		}

		return *this;
	}

	/// Get the result of calling operator-=() on a copy of this Matrix.
	auto
	operator-(Matrix const& that) const& -> Matrix
	{
		if (&that == this) {
			return get_zero_matrix();
		}

		return Matrix{*this} -= that;
	}

	/// Get the result of calling operator-=() on this rvalue Matrix.
	auto
	operator-(Matrix const& that) && -> Matrix
	{
		return std::move(*this -= that);
	}

	/// Get the result of calling operator-=() on this rvalue Matrix.
	auto
	operator-(Matrix&& that) && -> Matrix
	{
		return std::move(*this -= that);
	}

	/// Get the result of calling operator-=() on @p that rvalue Matrix.
	auto
	operator-(Matrix&& that) const& -> Matrix
	{
		return std::move(that -= *this);
	}


	/// Multiply each element of this Matrix by a scalar.
	auto
	operator*=(value_type const scalar) noexcept -> Matrix&
	{
		if (scalar == 0) {
			zero_this();
		} else if (scalar != 1) {
			transform(std::multiplies<value_type>{}, scalar);
		}

		return *this;
	}

	/// Get the result of calling operator*=() on a copy of this Matrix.
	auto
	operator*(value_type const scalar) const& noexcept -> Matrix
	{
		if (scalar == 0) {
			return get_zero_matrix();
		}

		return Matrix{*this} *= scalar;
	}

	/// Get the result of calling operator*=() on this rvalue Matrix.
	auto
	operator*(value_type const scalar) && noexcept -> Matrix
	{
		return std::move(*this *= scalar);
	}


	/** Divide each element of this Matrix by a scalar.
	  * @throw std::invalid_argument if the @p scalar is 0
	  */
	auto
	operator/=(value_type const scalar) -> Matrix&
	{
		ensure_scalar(scalar);

		if (scalar != 1) {
			transform(std::divides<value_type>{}, scalar);
		}

		return *this;
	}

	/** Get the result of calling operator/=() on a copy of this Matrix.
	  * @throw std::invalid_argument if the @p scalar is 0
	  */
	auto
	operator/(value_type const scalar) const& -> Matrix
	{
		return Matrix{*this} /= scalar;
	}

	/** Get the result of calling operator/=() on this rvalue Matrix.
	  * @throw std::invalid_argument if the @p scalar is 0
	  */
	auto
	operator/(value_type const scalar) && -> Matrix
	{
		return std::move(*this /= scalar);
	}


	/** Get the result of matrix multiplication of this Matrix by another.
	  *
	  * The number of columns in this Matrix must be equal to the number of
	  * rows in @p that one.
	  *
	  * @param that the other Matrix
	  *
	  * @throw std::domain_error if the dimensions are not compatible
	  */
	auto
	operator*(Matrix const& that) const -> Matrix
	{
		ensure<std::domain_error>(m_width == that.m_height,
		                          "width of this must == height of operand");

		auto ab = Matrix{ Sizes{m_height, that.m_width} };

		// ab(i, k) += a(i, j) * b(j, k);

		for (auto [i, p_a_ij, i_p_ab_ik] = std::tuple{ m_height, pbegin(), ab.pbegin() };
		     i--; i_p_ab_ik += ab.m_width)
		{
			for (auto [j, p_b_jk] = std::tuple{ m_width, that.pbegin() }; j--; ++p_a_ij)
			{
				auto const a_ij = *p_a_ij;

				if (a_ij == 0) {
					p_b_jk += ab.m_width;
					continue;
				}

				for (auto [k, p_ab_ik] = std::tuple{that.m_width, i_p_ab_ik};
				     k--; ++p_ab_ik, ++p_b_jk)
				{
					*p_ab_ik += a_ij * *p_b_jk;
				}
			}
		}

		return ab;
	}


	/** Get a submatrix of this Matrix.
	  *
	  * @param row the first row to extract
	  * @param col the first column to extract
	  * @param height the number of rows from @p row to extract
	  * @param width  the number of columns from @p col to extract
	  *
	  * @return the submatrix
	  *
	  * @throw std::invalid_argument if @p height or @p width are not >= 1
	  * @throw std::out_of_range if @p row or @p col exceed their bound
	  */
	auto
	get_submatrix(size_type const row   , size_type const col  ,
	              size_type const height, size_type const width) const -> Matrix
	{
		if (row == 0 and col == 0 and height == m_height and width == m_width) {
			return *this;
		}

		validate_size(height, width);
		ensure<std::out_of_range>(row + height <= m_height, "row + height is out of bounds");
		ensure<std::out_of_range>(col + width  <= m_width , "col + width  is out of bounds");

		auto elements = reserve_elements(height * width);

		for (auto [h, next_row, p] = std::tuple{ height, m_width - width, ap(row, col) };
		     h--; p += next_row)
		{
			copy_cols(p, width, elements);
		}

		return Matrix{ Sizes{height, width}, std::move(elements) };
	}

	/** Get the Matrix resulting from removing the specified row and column
	  * from this Matrix (and shifting the other elements accordingly).
	  *
	  * @param row the row to exclude
	  * @param col the column to exclude
	  *
	  * @return a Matrix of the elements from this Matrix, excepting those
	  *         whose position falls within @p row and/or @p col
	  *
	  * @throw std::domain_error if this Matrix has width or height of 1
	  * @throw std::out_of_range if @p row or @p col exceed their bound
	  */
	auto
	get_matrix_except(size_type const row, size_type const col) const -> Matrix
	{
		ensure<std::domain_error>(m_height >= 2, "makes no sense if height < 2!");
		ensure<std::domain_error>(m_width  >= 2, "makes no sense if width  < 2!");

		validate_row(row);
		validate_col(col);
		return get_matrix_except_private(row, col);
	}


	/** If this Matrix is_square(), get its determinant.
	  *
	  * @return the determinant
	  *
	  * @throw std::domain_error if this Matrix is not square
	  */
	auto
	get_determinant() const& -> value_type
	{
		return get_determinant_private();
	}

	/// This alters elements of this rvalue Matrix to do the calculation.
	/// @copydoc Matrix.get_determinant()
	auto
	get_determinant() && -> value_type
	{
		ensure_square();
		return std::move(*this).get_determinant_private();
	}


	/** Get a specified minor of this Matrix.
	  *
	  * @param row the row to remove
	  * @param col the column to remove
	  *
	  * The minor is the determinant of the Matrix that results from
	  * removing the given row and column from the original Matrix.
	  *
	  * @return the minor
	  *
	  * @throw std::domain_error if this Matrix is not square
	  */
	auto
	get_minor(size_type const row, size_type const col) const -> value_type
	{
		ensure_square();

		return get_minor_private(row, col);
	}

	/** Get the minors of this Matrix.
	  * @throw std::domain_error if this Matrix is not square
	  */
	auto
	get_minors() const -> Matrix
	{
		ensure_square();

		auto elements = reserve_elements();

		for (auto row = size_type{0}; row < m_height; ++row) {
			for (auto col = size_type{0}; col < m_width; ++col) {
				elements.emplace_back( get_minor_private(row, col) );
			}
		}

		return Matrix{ Sizes{m_height, m_width}, std::move(elements) };
	}

	/** Get the Matrix of cofactors for this Matrix.
	  * @throw std::domain_error if this Matrix is not square
	  */
	auto
	get_cofactors() const -> Matrix
	{
		ensure_square();

		return get_cofactors_private();
	}

	/** Get the adjugate of this Matrix i.e. the transpose of its cofactors.
	  * @throw std::domain_error if this Matrix is not square
	  */
	auto
	get_adjugate() const -> Matrix
	{
		ensure_square();

		return get_adjugate_private();
	}

	/// Get the transposition of this Matrix.
	auto
	get_transpose() const& noexcept -> Matrix
	{
		auto elements = reserve_elements();

		for (auto [c, pc] = std::tuple{ m_width, pbegin() };
		     c--; ++pc)
		{
			for (auto [r, pr] = std::tuple{m_height, pc};
			     r--; pr += m_width)
			{
				elements.emplace_back(*pr);
			}
		}

		return Matrix{ Sizes{m_width, m_height}, std::move(elements) };
	}

	/// @copybrief Matrix::get_transpose
	auto
	get_transpose() && noexcept -> Matrix
	{
		if ( is_square() ) {
			transpose_this_square();
			return std::move(*this);
		}

		return get_transpose();
	}

	/** If this Matrix is_square(), get its inverse Matrix.
	  *
	  * @return the inverse of this Matrix
	  *
	  * @throw std::domain_error if this Matrix is not square
	  * @throw std::domain_error if this Matrix has a get_determinant() of 0
	  */
	auto
	get_inverse() const -> Matrix
	{
		ensure_square();

		auto const determinant = get_determinant_private();
		ensure<std::domain_error>(determinant != 0,
		                          "A matrix with determinant == 0 cannot be inverted");
		return get_adjugate_private() /= determinant;
	}

	/// the result of elements_cast(), to avoid copying if not changing type
	template <typename T_NewElement, bool T_Const>
	using elements_cast_result = std::conditional_t< std::same_as<T_NewElement, value_type>,
		std::conditional_t<T_Const, Matrix const, Matrix>&, Matrix<T_NewElement> >;

	/** Get a Matrix of same dimension with elements cast to a given type.
	  *
	  * @return a Matrix with elements of this Matrix cast to T_NewElement,
	  *         or reference to self if T_NewElement equals @ref value_type.
	  */
	template<typename T_NewElement>
	auto
	elements_cast() const noexcept -> elements_cast_result<T_NewElement, true>
	{
		if constexpr ( std::same_as<T_NewElement, value_type> ) return *this;
		else return Matrix<T_NewElement>{ m_height, m_width,
		                                  std::vector<T_NewElement>{ pbegin(), pend() } };
	}

	/// @copydoc elements_cast
	template<typename T_NewElement>
	auto
	elements_cast() noexcept -> elements_cast_result<T_NewElement, false>
	{
		if constexpr ( std::same_as<T_NewElement, value_type> ) return *this;
		else return std::as_const(*this).template elements_cast<T_NewElement>();
	}

	/// Print the elements of this Matrix at the corresponding positions.
	void
	print() const noexcept
	{
		for (auto row = size_type{0}; row < m_height; ++row) {
			for (auto col = size_type{0}; col < m_width; ++col) {
				std::cout << (*this)(row, col) << '\t';
			}

			std::cout << '\n';
		}

		std::cout << '\n';
	}


	/** Swap two rows in this Matrix.
	  *
	  * @param r1 the 1st row
	  * @param r2 the 2st row
	  *
	  * @throw std::out_of_range if @p r1 or @p r2 exceed their bound
	  */
	void
	swap_rows(size_type const r1, size_type const r2)
	{
		validate_row(r1);
		validate_row(r2);
		swap_rows_private(r1, r2);
	}

	/** Add a multiple of one row in this matrix to another.
	  *
	  * @param r1 the 1st row
	  * @param r2 the 2st row, in which the result is stored
	  * @param n  the number of multiples of @p r1 to add to @p r2
	  *
	  * @throw std::out_of_range if @p r1 or @p r2 exceed their bound
	  */
	void
	add_rows(size_type const r1, size_type const r2, value_type const n = 1)
	{
		validate_row(r1);
		validate_row(r2);
		add_rows_private(r1, r2, n);
	}

	/** Multiply a row in this Matrix by a non-zero scalar value.
	  *
	  * @param row the row to multiply
	  * @param scalar the factor by which to multiply
	  *
	  * @throw std::out_of_range if @p row exceeds its bound
	  * @throw std::invalid_argument if @p scalar is zero
	  */
	void
	multiply_row(size_type const row, value_type const scalar)
	{
		validate_row(row);
		ensure_scalar(scalar);
		multiply_row_private(row, scalar);
	}

	/** Divide a row in this Matrix by a non-zero scalar value.
	  *
	  * @param row the row to divide
	  * @param scalar the factor by which to divide
	  *
	  * @throw std::out_of_range if @p row exceeds its bound
	  * @throw std::invalid_argument if @p scalar is zero
	  */
	void
	divide_row(size_type const row, value_type const scalar)
	{
		validate_row(row);
		ensure_scalar(scalar);
		divide_row_private(row, scalar);
	}

private:
	size_type m_height;
	size_type m_width;
	elements_type m_elements;

	using Sizes = std::pair<size_type, size_type>;

	Matrix(Sizes const sizes,
	       elements_type elements,
	       bool const do_asserts = true) noexcept
	:
		m_height{sizes.first },
		m_width {sizes.second},
		m_elements{ std::move(elements) }
	{
		if (do_asserts) {
			assert(m_height >= 1);
			assert(m_width  >= 1);
			assert(m_elements.size() == m_height * m_width);
		}
	}

	Matrix(Sizes const sizes,
	       bool const do_asserts = true) noexcept
	: Matrix{sizes,
	         elements_type(sizes.first * sizes.second),
	         do_asserts}
	{
	}

	static void
	validate_size( size_type const height, size_type const width,
	               std::source_location const src_loc = std::source_location::current() )
	{
		ensure<std::invalid_argument>(height >= 1, "height must be >= 1", src_loc);
		ensure<std::invalid_argument>(width  >= 1, "width  must be >= 1", src_loc);
	}

	static auto
	validated_size( size_type const height, size_type const width,
	                std::source_location const src_loc = std::source_location::current() )
	{
		validate_size(height, width, src_loc);
		return std::pair{height, width};
	}

	void
	validate_row( size_type const row,
	              std::source_location const src_loc = std::source_location::current() ) const
	{
		ensure<std::out_of_range>(row < m_height, "row is out of bounds", src_loc);
	}

	void
	validate_col( size_type const col,
	              std::source_location const src_loc = std::source_location::current() ) const
	{
		ensure<std::out_of_range>(col < m_width, "column is out of bounds", src_loc);
	}

	void
	ensure_scalar( value_type const scalar,
	               std::source_location const src_loc = std::source_location::current() ) const
	{
		ensure<std::invalid_argument>(scalar != 0, "scalar cannot be 0", src_loc);
	}

	void
	ensure_square( std::source_location const src_loc = std::source_location::current() ) const
	{
		ensure<std::domain_error>(is_square(), "matrix must be square", src_loc);
	}

	static auto
	reserve_elements(size_type const size) noexcept -> elements_type
	{
		auto elements = elements_type{};
		elements.reserve(size);
		return elements;
	}

	auto
	reserve_elements() const noexcept -> elements_type
	{
		return reserve_elements( m_elements.size() );
	}

	auto
	ap(size_type const row, size_type const col) const -> value_type const*
	{
		return &(*this)(row, col);
	}

	auto
	pbegin() const noexcept -> value_type const*
	{
		return m_elements.data();
	}

	auto
	pend() const noexcept -> value_type const*
	{
		return pbegin() + m_elements.size();
	}

	auto
	ap(size_type const row, size_type const col) -> value_type*
	{
		return &(*this)(row, col);
	}

	auto
	pbegin() noexcept -> value_type*
	{
		return m_elements.data();
	}

	auto
	pend() noexcept -> value_type*
	{
		return pbegin() + m_elements.size();
	}

	void
	zero_this() noexcept
	{
		std::fill(pbegin(), pend(), 0);
	}

	void
	iden_this() noexcept
	{
		for (auto [i, diagonal, p] = std::tuple{ m_height, m_width + 1, pbegin() };
		     i--; p += diagonal)
		{
			*p = 1;
		}
	}

	void
	transpose_this_square() noexcept
	{
		assert( is_square() );
		for (auto row = size_type{0}; row < m_height; ++row) {
			for (auto col = size_type{0}; col < row; ++col) {
				std::swap( (*this)(row, col), (*this)(col, row) );
			}
		}
	}

	static void
	copy_cols(value_type const*& p, size_type cols,
	          elements_type& elements) noexcept
	{
		while (cols--) {
			elements.emplace_back(*p++);
		}
	}

	static void
	copy_rows_except(value_type const*& p, size_type rows,
	                 size_type const col, size_type const end_cols,
	                 elements_type& elements) noexcept
	{
		while (rows--) {
			copy_cols(p, col     , elements);
			++p;
			copy_cols(p, end_cols, elements);
		}
	}

	auto
	get_matrix_except_private(size_type const row, size_type const col) const noexcept -> Matrix
	{
		// It makes no sense to ask to remove the only row or column
		assert(m_height > 1);
		assert(m_width  > 1);

		auto const result_rows = m_height - 1;
		auto const result_cols = m_width  - 1;
		auto const end_cols = result_cols - col;
		auto const end_rows = result_rows - row;
		auto elements = reserve_elements(result_rows * result_cols);
		auto p = pbegin();

		copy_rows_except(p, row     , col, end_cols, elements);
		p += m_width;
		copy_rows_except(p, end_rows, col, end_cols, elements);

		return Matrix{ Sizes{result_rows, result_cols}, std::move(elements) };
	}

	auto
	get_minor_private(size_type const row, size_type const col) const noexcept -> value_type
	{
		assert( is_square() );

		return get_matrix_except_private(row, col).get_determinant_private();
	}

	auto
	get_cofactor(size_type const row, size_type const col) const noexcept -> value_type
	{
		auto const minor = get_minor_private(row, col);
		return (row + col) & 1 ? -minor : minor;
	}

	auto
	get_cofactors_private() const noexcept -> Matrix
	{
		return get_cofactors_or_adjugate_private(false);
	}

	auto
	get_adjugate_private() const noexcept -> Matrix
	{
		return get_cofactors_or_adjugate_private(true);
	}

	auto
	get_cofactors_or_adjugate_private(bool const transpose) const noexcept -> Matrix
	{
		assert( is_square() );

		auto elements = reserve_elements();

		for (auto row = size_type{0}; row < m_height; ++row) {
			for (auto col = size_type{0}; col < m_width; ++col) {
				elements.emplace_back( transpose ? get_cofactor(col, row)
				                                 : get_cofactor(row, col) );
			}
		}

		return Matrix{ Sizes{m_height, m_width}, std::move(elements) };
	}

	auto
	bareiss_non0row(size_type const dim, size_type const k) const noexcept
	-> std::optional< std::tuple<size_type, value_type> >
	{
		auto m = k + 1;

		for (auto p = ap(m, k); m < dim; ++m, p += dim) {
			if (auto const mk = *p; mk != 0) {
				return std::tuple{m, mk};
			}
		}

		return std::nullopt;
	}

	auto
	bareiss() && noexcept -> value_type
	{
		auto const dim = m_width;
		auto sign = value_type{1};
		auto pkk = pbegin();

		for (auto [k, kk, lkk] = std::tuple{ size_type{0}, value_type{}, value_type{1} };
		     k < dim - 1; ++k, pkk += dim + 1)
		{
			kk = *pkk;

			if (kk == 0) {
				auto const non0row = bareiss_non0row(dim, k);

				if (non0row == std::nullopt) {
					return 0;
				}

				auto const& [m, mk] = *non0row;
				swap_rows(m, k);
				sign = -sign;
				kk = mk;
			}

			auto i = k + 1;

			for (auto ik = ap(i, k); i < dim; ++i, ik += dim) {
				auto j = k + 1;

				for (auto ij = ap(i, j), kj = ap(k, j);
				     j < dim; ++j, ++ij, ++kj)
				{
					*ij = (kk * *ij - *ik * *kj) / lkk;
				}
			}

			lkk = kk;
		}

		return sign * *pkk;
	}

	auto
	get_determinant_private() && noexcept -> value_type
	{
		assert( is_square() );
		return std::move(*this).bareiss();
	}

	auto
	get_determinant_private() const& noexcept -> value_type
	{
		return Matrix{*this}.get_determinant_private();
	}

	void
	swap_rows_private(size_type const r1, size_type const r2) noexcept
	{
		if (r1 == r2) {
			return;
		}

		auto const p1 = ap(r1, 0);
		auto const p2 = ap(r2, 0);
		std::swap_ranges(p1, p1 + m_width, p2);
	}

	void
	add_rows_private(size_type const r1, size_type const r2, value_type const n) noexcept
	{
		if (n == 0) {
			return;
		}

		auto const* const p1 = ap(r1, 0);
		auto        const p2 = ap(r2, 0);
		std::transform( p1, p1 + m_width, p2,
		                p2, [=](auto a, auto b){ return std::move(b += a *= n); } );
	}

	void
	multiply_row_private(size_type const row, value_type const scalar) noexcept
	{
		if (scalar != 1) {
			transform(row, std::multiplies<value_type>{}, scalar);
		}
	}

	void
	divide_row_private(size_type const row, value_type const scalar) noexcept
	{
		if (scalar != 1) {
			transform(row, std::divides<value_type>{}, scalar);
		}
	}

	template <std::invocable<value_type, value_type> T_Func>
	void
	transform(value_type* const first, value_type* const last,
	          T_Func const& func, value_type const scalar) noexcept
	{
		std::transform( first, last,
		                first, std::bind(func, std::placeholders::_1, scalar) );
	}

	template <std::invocable<value_type, value_type> T_Func>
	void
	transform(size_type const row, T_Func const& func, value_type const scalar) noexcept
	{
		auto const p = ap(row, 0);
		transform(p, p + m_width, func, scalar);
	}

	template <std::invocable<value_type, value_type> T_Func>
	void
	transform(T_Func const& func, value_type const scalar) noexcept
	{
		transform(pbegin(), pend(), func, scalar);
	}

	template <std::invocable<value_type, value_type> T_Func>
	void
	transform(T_Func const& func, Matrix const& that)
	{
		ensure<std::domain_error>(has_same_dimension(that),
		                          "sides must have same dimensions");

		auto const pb = pbegin();
		std::transform(pb, pend(), that.pbegin(),
		               pb, func);
	}
};

template <typename T_Value> Matrix( std::size_t, std::size_t,
                                    std::initializer_list<T_Value> ) -> Matrix<T_Value>;
template <typename T_Value> Matrix( std::size_t, std::size_t,
                                    std::span<T_Value> ) -> Matrix<T_Value>;
template <typename T_Value> Matrix(std::size_t, std::size_t,
                                   std::vector<T_Value> const&) -> Matrix<T_Value>;

/// See Matrix::operator*(), for which this provides symmetry.
template <typename T_Element>
auto
operator*(typename Matrix<T_Element>::value_type const  scalar,
                   Matrix<T_Element>             const& matrix) noexcept
{
	return matrix * scalar;
}

/// See Matrix::operator*(), for which this provides symmetry.
template <typename T_Element>
auto
operator*(typename Matrix<T_Element>::value_type const scalar,
                   Matrix<T_Element>                && matrix) noexcept
{
	return std::move(matrix *= scalar);
}

} // namespace matrix

using matrix::Matrix;

} // namespace djb

#endif // DJB_MATRIX_HPP
