#ifndef DJB_MATRIX_ENSURE_HPP
#define DJB_MATRIX_ENSURE_HPP

#include <format>
#include <string>
#include <source_location>
#include <utility>

namespace djb::matrix {

template<typename T_Exception>
static void
ensure( bool const condition, char const* const message,
	std::source_location const src_loc = std::source_location::current() )
{
	if (condition) return;
	auto what = std::format("\n{0}:{1}\n{2}()\n{3}",
	                        src_loc.file_name(), src_loc.line(),
	                        src_loc.function_name(),
	                        message);
	throw T_Exception{ std::move(what) };
}

} // namespace djb::matrix

#endif // DJB_MATRIX_ENSURE_HPP
