#include "Matrix.hpp"
#include <cstdlib>

using namespace djb;

static auto ints    = Matrix<int   >{ 3, 2, {4, 8, 15, 16, 23, 42} };
static auto doubles = Matrix<double>{ 3, 2, {4, 8, 15, 16, 23, 42} };

static void
test_memb_casts()
{
	decltype(auto) ints2    = doubles.elements_cast<int   >();
	decltype(auto) doubles2 = ints   .elements_cast<double>();
	static_assert( not std::is_reference_v< decltype(ints2   ) > );
	static_assert( not std::is_reference_v< decltype(doubles2) > );

	if (ints2 != ints) {
		std::exit(1);
	}

	if (doubles2 != doubles) {
		std::exit(2);
	}
}

static void
test_noop_casts()
{
	decltype(auto) ints2    = ints   .elements_cast<int   >();
	decltype(auto) doubles2 = doubles.elements_cast<double>();
	static_assert( std::is_reference_v< decltype(ints2   ) > );
	static_assert( std::is_reference_v< decltype(doubles2) > );

	if (&ints2 != &ints) {
		std::exit(3);
	}

	if (&doubles2 != &doubles) {
		std::exit(4);
	}
}

auto
main() -> int
{
	test_memb_casts();
	test_noop_casts();

	return 0;
}
