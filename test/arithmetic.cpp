#include "Matrix.hpp"
#include "test.hpp"

using namespace djb;

static void
test_multiply(Matrix<double> const& ab,
              Matrix<double> const& correct_ab)
{
	test( ab.equal_below_epsilon(correct_ab, 0.000000001) );
}

static void
test_multiply(Matrix<double> const& a,
              Matrix<double> const& b,
              Matrix<double> const& correct_ab)
{
	test_multiply(       a  *        b , correct_ab);
	test_multiply(Matrix{a} *        b , correct_ab);
	test_multiply(       a  * Matrix{b}, correct_ab);
	test_multiply(Matrix{a} * Matrix{b}, correct_ab);
}

auto
main() -> int
{
	/* Most example data was taken from here:
	 * https://www.intmath.com/matrices-determinants/4-multiplying-matrices.php
	 */


	// operator*=

	auto const a = Matrix<double>{ 2u, 3u, {
		 0, -1, 2,
		 4, 11, 2
	} };

	auto const z = a.get_zero_matrix();

	auto const a_times_two = Matrix<double>{ 2u, 3u, {
		0, -2, 4,
		8, 22, 4
	} };

	auto const a_times_three = Matrix<double>{ 2u, 3u, {
		 0, -3, 6,
		12, 33, 6
	} };

	auto a2 = a;
	a2 *= 1;
	test(a2 == a);

	a2 *= 2;
	test(a2 == a_times_two);

	a2 *= 1.5;
	test(a2 == a_times_three);

	a2 *= 2./3;
	test(a2 == a_times_two);

	a2 *= 0.5;
	test(a2 == a);

	a2 *= 0;
	test(a2 == z);


	// member operator*

	a2 = a * 1;
	test(a2 == a);

	a2 = a2 * 2;
	test(a2 == a_times_two);

	a2 = a2 * 1.5;
	test(a2 == a_times_three);

	a2 = a2 * 2./3;
	test(a2 == a_times_two);

	a2 = a2 * 0.5;
	test(a2 == a);

	a2 = a2 * 0;
	test(a2 == z);

	test(Matrix{a} * 2 == a_times_two);


	// free operator*

	a2 = 1 * a;
	test(a2 == a);

	a2 = 2 * a2;
	test(a2 == a_times_two);

	a2 = 1.5 * a2;
	test(a2 == a_times_three);

	a2 = 2./3 * a2;
	test(a2 == a_times_two);

	a2 = 0.5 * a2;
	test(a2 == a);

	a2 = 0 * a2;
	test(a2 == z);

	test(2 * Matrix{a} == a_times_two);


	// division by a scalar

	a2 = a;
	a2 /= 1;
	test(a2 == a);

	a2 /= 0.5;
	test(a2 == a_times_two);

	a2 /= 2./3;
	test(a2 == a_times_three);

	a2 /= 3./2;
	test(a2 == a_times_two);

	a2 /= 2;
	test(a2 == a);

	a2 = a2 / 1;
	test(a2 == a);

	a2 = a2 / 0.5;
	test(a2 == a_times_two);

	a2 = a2 / (2./3);
	test(a2 == a_times_three);

	a2 = a2 / (3./2);
	test(a2 == a_times_two);

	a2 = a2 / 2;
	test(a2 == a);

	try {
		a2 /= 0;
		test(false);
	}
	catch (std::invalid_argument const&) {}

	try {
		a2 = a2 / 0;
		test(false);
	}
	catch (std::invalid_argument const&) {}

	test(Matrix{a_times_two} / 2 == a);


	// add/subtract matrices

	a2 += a;
	test(a2 == a_times_two);

	a2 = a2 + a;
	test(a2 == a_times_three);

	a2 -= a;
	test(a2 == a_times_two);

	a2 = a2 - a;
	test(a2 == a);

	a2 += a2;
	test(a2 == a_times_two);

	a2 = a;
	a2 -= a;
	test(a2 == z);

	a2 = a;
	a2 = a2 - a;
	test(a2 == z);

	test(       a  + Matrix{a} == a_times_two);
	test(Matrix{a} +        a  == a_times_two);
	test(Matrix{a} + Matrix{a} == a_times_two);

	test(       a  - Matrix{a} == z);
	test(Matrix{a} -        a  == z);
	test(Matrix{a} - Matrix{a} == z);


	// multiplication of matrices

	auto const b = Matrix<double>{ 3u, 2u, {
		 3, -1,
		 1,  2,
		 6,  1,
	} };

	auto const correct_ab = Matrix<double>{ 2u, 2u, {
		11,  0,
		35, 20
	} };

	test_multiply(a, b, correct_ab);

	auto const c = Matrix<double>{ 2u, 2u, {
		 8,  9,
		 5, -1
	} };

	auto const d = Matrix<double>{ 2u, 2u, {
		-2,  3,
		 4,  0,
	} };

	auto const correct_cd = Matrix<double>{ 2u, 2u, {
		 20, 24,
		-14, 15
	} };

	test_multiply(c, d, correct_cd);


	// multiplication by the identity matrix

	auto const e = Matrix<double>{ 3u, 3u, {
		 7, 13, 21,
		32, 42, 47,
		77, 89, 99
	} };

	auto const i = e.get_identity_matrix();

	test_multiply(e, i, e);


	// multiplication by the inverse matrix should return I

	auto const f = Matrix<double>{ 3u, 3u, {
		1, 0, 3,
		2, 2, 2,
		1, 3, 0
	} };

	auto const f_1 = f.get_inverse();

	test_multiply(f, f_1, i);


	// multiplication is not commutative

	test_multiply(
		{ 2, 2, {1, 2,
		         3, 4} },
		{ 2, 2, {0, 1,
		         0, 0} },
		{ 2, 2, {0, 1,
		         0, 3} } );

	test_multiply(
		{ 2, 2, {0, 1,
		         0, 0} },
		{ 2, 2, {1, 2,
		         3, 4} },
		{ 2, 2, {3, 4,
		         0, 0} } );


	return 0;
}
