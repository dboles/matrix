#include "Matrix.hpp"
#include "test.hpp"

#include <cmath>
#include <utility>
#include <vector>

using namespace djb;
using namespace std;
using Vec = vector<double>;
using In = vector< pair<Vec, double> >;

[[nodiscard]] static auto
ri_to_rx_ry(In const& ri)
{
	auto const m = ri.at(0).first.size();
	auto const n = ri.size();
	auto rxv = Vec{};
	auto ryv = Vec{}; 
	rxv.reserve( n * (1 + m) );
	ryv.reserve( n           );
	for (auto const rxvi = back_inserter(rxv);
	     auto const& [rx, ry]: ri)
	{
		rxv.push_back(1 ); ranges::copy(rx, rxvi);
		ryv.push_back(ry);
	}
	auto rx = Matrix{ n, 1 + m, move(rxv) };
	auto ry = Matrix{ n, 1    , move(ryv) };
	return pair{ move(rx), move(ry) };
}

[[nodiscard]] static auto
qxv_to_qx(Vec qxv)
{
	qxv.insert(qxv.begin(), 1);
	return Matrix{ 1, qxv.size(), move(qxv) };
}

static void
test(In const& ri, In qi)
{
	auto const [rx, ry] = ri_to_rx_ry(ri);
	auto const rxt = rx.get_transpose();
	auto const rb = (rxt * rx).get_inverse() * rxt * ry;
	for (auto [qxv, qye]: qi) {
		auto const qx = qxv_to_qx( move(qxv) );
		auto const qya = qx * rb;
		test(qya.get_height() == 1);
		test(qya.get_width () == 1);
		test(abs(qya.at(0, 0) - qye) <= 0.01);
	}
}

auto
main() -> int
{
	// https://www.hackerrank.com/challenges/s10-multiple-linear-regression/tutorial
	test(
		// ri:
		{ { {5, 7}, 10 },
		  { {6, 6}, 20 },
		  { {7, 4}, 60 },
		  { {8, 5}, 40 },
		  { {9, 6}, 50 } },
		// qi:
		{ { {5, 5}, 29.39535} }
	);

	// https://www.hackerrank.com/challenges/s10-multiple-linear-regression/problem
	test(
		// ri:
		{ { {0.18, 0.89}, 109.85 },
		  { {1.00, 0.26}, 155.72 },
		  { {0.92, 0.11}, 137.66 },
		  { {0.07, 0.37},  76.17 },
		  { {0.85, 0.16}, 139.75 },
		  { {0.99, 0.41}, 162.60 },
		  { {0.87, 0.47}, 151.77 } },
		// qi:
		{ { {0.49, 0.18}, 105.22 },
		  { {0.57, 0.83}, 142.68 },
		  { {0.56, 0.64}, 132.94 },
		  { {0.76, 0.18}, 129.71 } }
	);

	return 0;
}
