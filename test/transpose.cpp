#include "Matrix.hpp"
#include "test.hpp"

#include <cstdlib>
#include <ctime>
#include <utility>

using namespace djb;

static void
test(Matrix<int> const& matrix,
     Matrix<int> const& transpose)
{
	// Transpose
	auto result = matrix.get_transpose();
	test(result == transpose);

	// Transpose back
	result = std::move(result).get_transpose();
	test(result == matrix);
}

auto
main() -> int
{
	std::srand( std::time(nullptr) );
	auto a = Matrix{ 1, 1, { std::rand() } };
	test(a, a);


	a = { 2, 2, {
		1, 2,
		3, 4,
	} };

	auto b = Matrix{ 2, 2, {
		1, 3,
		2, 4,
	} };

	test(a, b);


	a = { 2, 3, {
		1, 2, 3,
		4, 5, 6,
	} };

	b = Matrix{ 3, 2, {
		1, 4,
		2, 5,
		3, 6,
	} };

	test(a, b);


	a = { 3, 3, {
		1, 2, 3,
		4, 5, 6,
		7, 8, 9,
	} };

	b = Matrix{ 3, 3, {
		1, 4, 7,
		2, 5, 8,
		3, 6, 9,
	} };

	test(a, b);

	a = std::move(a).get_zero_matrix();
	test(a, a);


	a = { 4, 1, {
		1,
		2,
		3,
		4,
	} };

	b = Matrix{ 1, 4, {
		1, 2, 3, 4,
	} };

	test(a, b);


	return 0;
}
