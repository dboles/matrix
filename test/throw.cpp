#include "Matrix.hpp"
#include "test_throws.hpp"

auto
main() -> int
{
	using Matrix = djb::Matrix<int>;
	using Elements = Matrix::elements_type;
	using djb::test_ctor_throws;

	test_ctor_throws<Matrix, std::invalid_argument>( true , 0, 0                          );
	test_ctor_throws<Matrix, std::invalid_argument>( true , 1, 0                          );
	test_ctor_throws<Matrix, std::invalid_argument>( true , 0, 1                          );
	test_ctor_throws<Matrix, std::invalid_argument>( true , 0, 1                          );
	test_ctor_throws<Matrix, std::invalid_argument>( false, 1, 1                          );
	test_ctor_throws<Matrix, std::invalid_argument>( false, 2, 2                          );
	test_ctor_throws<Matrix, std::length_error    >( true , 2, 2, Elements{             } );
	test_ctor_throws<Matrix, std::length_error    >( true , 2, 2, Elements{1            } );
	test_ctor_throws<Matrix, std::length_error    >( true , 2, 2, Elements{1, 2         } );
	test_ctor_throws<Matrix, std::length_error    >( true , 2, 2, Elements{1, 2, 3      } );
	test_ctor_throws<Matrix, std::length_error    >( false, 2, 2, Elements{1, 2, 3, 4   } );
	test_ctor_throws<Matrix, std::length_error    >( true , 2, 2, Elements{1, 2, 3, 4, 5} );

	// FIXME: test other exceptions

	return 0;
}
