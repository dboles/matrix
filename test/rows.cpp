#include "Matrix.hpp"
#include "test.hpp"

using namespace djb;

static void
test_swap(Matrix<int> const& original, int const r1, int const r2,
          Matrix<int> const& expected)
{
	auto matrix = original;

	matrix.swap_rows(r1, r2);
	test(matrix == expected);

	matrix.swap_rows(r1, r2);
	test(matrix == original);
}

static void
test_add(Matrix<int> const& original, int const r1, int const r2, int const f,
         Matrix<int> const& expected)
{
	auto matrix = original;
	matrix.add_rows(r1, r2, f);
	test(matrix == expected);
}

static void
test_mul(Matrix<int> const& original, int const row, int const factor,
          Matrix<int> const& expected)
{
	auto matrix = original;
	matrix.multiply_row(row, factor);
	test(matrix == expected);
}

static void
test_div(Matrix<int> const& original, int const row, int const divisor,
         Matrix<int> const& expected)
{
	auto matrix = original;
	matrix.divide_row(row, divisor);
	test(matrix == expected);
}

auto
main() -> int
{
	/* from https://www.khanacademy.org/math/precalculus
	                /x9e81a4f98389efdf:matrices
	                /x9e81a4f98389efdf:elementary-matrix-row-operations
	                /a/matrix-row-operations
	*/


	auto original = Matrix{ 3, 3, {4, 8, 3,
	                               2, 4, 5,
	                               7, 1, 2} };
	auto expected = Matrix{ 3, 3, {2, 4, 5,
	                               4, 8, 3,
	                               7, 1, 2} };
	test_swap(original, 0, 1, expected);

	original = expected;
	expected = { 3, 3, { 2,  4,  5,
	                     4,  8,  3,
	                    11,  9,  5} };
	test_add(original, 1, 2, 1, expected);

	original = expected;
	expected = { 3, 3, { -6, -12,  -1,
	                      4,   8,   3,
	                     11,   9,   5} };
	test_add(original, 1, 0, -2, expected);

	original = { 3, 3, { 7,  2,  9,
	                     6,  4,  1,
	                     1,  3, 12} };
	expected = { 3, 3, { 7,  2,  9,
	                     1,  3, 12,
	                     6,  4,  1} };
	test_swap(original, 1, 2, expected);


	original = { 3, 3, {6, 6, 1,
	                    2, 3, 0,
	                    4, 5, 9} };
	expected = { 3, 3, {6, 6, 1,
	                    6, 9, 0,
	                    4, 5, 9} };
	test_mul(original, 1, 3, expected);
	test_div(expected, 1, 3, original);

	try {
		test_mul(original, 1, 0, expected);
		test(false);
	} catch (std::invalid_argument const&) {}

	try {
		test_div(original, 1, 0, expected);
		test(false);
	} catch (std::invalid_argument const&) {}


	return 0;
}
