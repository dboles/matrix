#ifndef DJB_TEST_THROWS_HPP
#define DJB_TEST_THROWS_HPP

#include "test.hpp"
#include <concepts>
#include <stdexcept>
#include <utility>

namespace djb {

template <std::derived_from<std::exception> Exception>
static void
test_throws(bool const should_throw, std::invocable auto const& function)
{
	auto did_throw = false;

	try { function(); }
	catch (Exception const&) { did_throw = true; }

	test(did_throw == should_throw);
}

template <typename CtorType, std::derived_from<std::exception> Exception, typename... Args>
static void
test_ctor_throws(bool const should_throw, Args... args)
{
	test_throws<Exception>( should_throw, [&]
	{
		static_cast<void>( CtorType(std::move(args)...) );
	} );
}

} // namespace djb

#endif // DJB_TEST_THROWS_HPP
