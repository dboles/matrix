#include "Matrix.hpp"
#include "test.hpp"

#include <cmath>
#include <initializer_list>

using namespace djb;

// N.B. need doubles for the right result (avoid truncation everywhere)
static void
test(Matrix<double> const& a, Matrix<double> const& c,
     std::initializer_list<double> const expected_terms)
{
	auto const n = expected_terms.size();
	test( a.get_height() == c.get_height() );
	test( a.get_width () == n );

	auto const x = a.get_inverse() * c;
	for (auto i = n; i--;) {
		// Round back to int to avoid more floating-point horror
		auto const term = std::lround( x(i, 0) );
		test( term == expected_terms.begin()[i] );
	}
}

auto
main() -> int
{
	// from https://www.mathplanet.com/education/algebra-2/matrices/using-matrices-when-solving-system-of-equations
	test(
		// A:
		{ 2, 2, {
			3,  1,
			2, -1,
		} },

		// C:
		{ 2, 1, {
			5,
			0,
		} },

		// x, y:
		{1, 2}
	);

	// from https://www.mathsisfun.com/algebra/systems-linear-equations-matrices.html
	test(
		// A:
		{ 3, 3, {
			1,  1,  1,
			0,  2,  5,
			2,  5, -1
		} },

		// C:
		{ 3, 1, {
			  6,
			- 4,
			 27
		} },

		// x, y, z:
		{5, 3, -2}
	);

	// from https://www.intmath.com/matrices-determinants/6-matrices-linear-equations.php
	test(
		// A:
		{ 3, 3, {
			 1,  2, -1,
			 3,  5, -1,
			-2, -1, -2
		} },

		// C:
		{ 3, 1, {
			6,
			2,
			4
		} },

		// x, y, z:
		{22, -16, -16}
	);

	return 0;
}
