#ifndef DJB_MATRIX_TEST_HPP
#define DJB_MATRIX_TEST_HPP

namespace djb {

void test(bool condition);

} // namespace djb

#endif // DJB_MATRIX_TEST_HPP
