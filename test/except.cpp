#include "Matrix.hpp"
#include "test.hpp"

using namespace djb;

static void
test_except(Matrix<int> const& matrix, Matrix<int>::size_type const top_col,
            Matrix<int> const& expected)
{
	test( top_col < matrix.get_width() );
	test(expected.get_height() == matrix.get_height() - 1);
	test(expected.get_width () == matrix.get_width () - 1);

	auto const split = matrix.get_matrix_except(0u, top_col);
	test(split == expected);
}

auto
main() -> int
{
	/* Most test data is from this fantastically grammatically named website:
	   https://www.mathsisfun.com/algebra/matrix-determinant.html */

	auto const matrix = Matrix<int>{4, 4, {
		0x0, 0x1, 0x2, 0x3,
		0x4, 0x5, 0x6, 0x7,
		0x8, 0x9, 0xA, 0xB,
		0xC, 0xD, 0xE, 0xF,
	} };

	test_except(matrix, 0,
		{3, 3, {
			0x5, 0x6, 0x7,
			0x9, 0xA, 0xB,
			0xD, 0xE, 0xF,
		} }
	);

	test_except(matrix, 1,
		{3, 3, {
			0x4, 0x6, 0x7,
			0x8, 0xA, 0xB,
			0xC, 0xE, 0xF,
		} }
	);

	test_except(matrix, 2,
		{3, 3, {
			0x4, 0x5, 0x7,
			0x8, 0x9, 0xB,
			0xC, 0xD, 0xF,
		} }
	);

	test_except(matrix, 3,
		{3, 3, {
			0x4, 0x5, 0x6,
			0x8, 0x9, 0xA,
			0xC, 0xD, 0xE,
		} }
	);

	return 0;
}
