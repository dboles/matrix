#include "Matrix.hpp"

using namespace djb;

auto
main() -> int
{
	auto const matrix = Matrix{ 4, 4, {0x0, 0x1, 0x2, 0x3,
	                                   0x4, 0x5, 0x6, 0x7,
	                                   0x8, 0x9, 0xA, 0xB,
	                                   0xC, 0xD, 0xE, 0xF} };


	// invalid size

	try {
		matrix.get_submatrix(0, 0, 0, 1);
		return 1;
	}
	catch (std::invalid_argument const&) {}


	try {
		matrix.get_submatrix(0, 0, 1, 0);
		return 2;
	}
	catch (std::invalid_argument const&) {}


	// squares

	if ( matrix.get_submatrix(0, 0, 2, 2) != Matrix{ 2, 2, {0x0, 0x1,
	                                                        0x4, 0x5} } )
	{
		return 3;
	}

	if ( matrix.get_submatrix(1, 1, 2, 2) != Matrix{ 2, 2, {0x5, 0x6,
	                                                        0x9, 0xA} } )
	{
		return 4;
	}

	if ( matrix.get_submatrix(2, 2, 2, 2) != Matrix{ 2, 2, {0xA, 0xB,
	                                                        0xE, 0xF} } )
	{
		return 5;
	}


	// out of bounds

	try {
		matrix.get_submatrix(3, 3, 2, 2);
		return 6;
	}
	catch (std::out_of_range const&) {}


	try {
		matrix.get_submatrix(2, 2, 3, 3);
		return 7;
	}
	catch (std::out_of_range const&) {}


	// 1 column
	if ( matrix.get_submatrix(0, 1, 4, 1) != Matrix{ 4, 1, {0x1,
	                                                        0x5,
	                                                        0x9,
	                                                        0xD} } )
	{
		return 8;
	}

	// 1 row
	if ( matrix.get_submatrix(1, 0, 1, 4) != Matrix{ 1, 4, {0x4, 0x5, 0x6, 0x7} } )
	{
		return 9;
	}


	return 0;
}
