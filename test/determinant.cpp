#include "Matrix.hpp"
#include "test.hpp"

#include <cstdlib>
#include <ctime>

using namespace djb;

static void
test_determinant(Matrix<int>&& matrix, int const expected_determinant)
{
	auto const determinant = std::move(matrix).get_determinant();
	test(determinant == expected_determinant);
}

auto
main() -> int
{
	/* Most test data is from this fantastically grammatically named website:
	   https://www.mathsisfun.com/algebra/matrix-determinant.html */


	// The determinant of a 1x1 matrix is simply its single element
	std::srand( std::time(nullptr) );
	auto const n = std::rand();
	test_determinant( {1, 1, {n} }, n );

	test_determinant( {2, 2, {4, 6,
	                          3, 8} }, 14 );

	test_determinant( {3, 3, {6,  1,  1,
	                          4, -2,  5,
	                          2,  8,  7} }, -306);


	// from https://people.richland.edu/james/lecture/m116/matrices/determinant.html

	test_determinant( {3, 3, {1, 3, 2,
	                          4, 1, 3,
	                          2, 5, 2} }, 17);

	test_determinant( {4, 4, {3, 2, 0, 1,
	                          4, 0, 1, 2,
	                          3, 0, 2, 1,
	                          9, 2, 3, 1} }, 24 );


	// from https://math.stackexchange.com/questions/1955784/how-to-find-the-determinant-of-a-5x5-matrix

	test_determinant( {5, 5, {  0,   6,  -2,  -1,   5,
	                            0,   0,   0,  -9,  -7,
	                            0,  15,  35,   0,   0,
	                            0,  -1, -11,  -2,   1,
	                           -2,  -2,   3,   0,  -2} }, 2480 );


	// Test concluding zero when we should

	/* trivial, from https://math.stackexchange.com/questions/355644
	         *              /what-does-it-mean-to-have-a-determinant-equal-to-zero#comment1365273_355647
	 */

	test_determinant( { 3, 3, {1, 2, 3,
	                           4, 5, 6,
	                           5, 7, 9} }, 0 );

	// sneaky, from benchmark/inverse.cpp, fails with int Laplace but passes with int Bareiss

	test_determinant( { 10, 10, {41, 32, 73, 17, 94, 51, 29, 20, 47, 80,
	                             21, 41, 46, 67, 93, 37, 85, 13,  4, 16,
	                             67,  1, 17, 68, 37,  4,  4, 95, 53, 57,
	                             42, 67, 24, 67, 64, 18, 30, 49,  8, 18,
	                             72, 28, 87, 46, 47, 51, 25, 70, 68, 24,
	                             51, 19, 59, 23, 39, 0,  95, 66, 56, 50,
	                             31,  5,  3, 80, 41, 43, 78, 74, 49, 56,
	                             24, 36, 41, 82, 41, 45, 17, 36, 76, 51,
	                             51, 91, 78, 90, 84, 13,  5,  7, 78, 98,
	                             21, 67, 20,  0,  2, 26, 24, 35, 62,  6} }, 0 );


	return 0;
}
