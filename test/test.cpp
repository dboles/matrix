#include "test.hpp"
#include <cstdlib>

namespace djb {

void
test(bool const condition)
{
	static auto f_test_number = 0;

	++f_test_number;

	if (not condition) {
		std::exit(f_test_number);
	}
}

} // namespace djb
