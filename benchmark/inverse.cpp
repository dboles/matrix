#include "common.hpp"

auto
main() -> int
{
	using namespace djb::matrix::benchmark;
	auto constexpr c_size = 69;

	return measure(
		// setup
		[]
		{
			while (true) {
				auto matrix = create_random_matrix(c_size, c_size);

				if ( matrix.get_determinant() != 0 ) {
					return matrix;
				}
			}
		},

		// work
		[](auto const& matrix)
		{
			return matrix.get_inverse();
		}
	);
}
