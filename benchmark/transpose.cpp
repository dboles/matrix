#include "common.hpp"

auto
main() -> int
{
	using namespace djb::matrix::benchmark;
	auto constexpr c_size = 10000;

	return measure(
		// setup
		[]
		{
			return create_random_matrix(c_size, c_size);
		},

		// work
		[](auto const& matrix)
		{
			return matrix.get_transpose();
		}
	);
}
