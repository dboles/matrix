#include "common.hpp"
#include <array>

auto
main() -> int
{
	using namespace djb::matrix::benchmark;
	using matrices_type = std::array<djb::Matrix<int>, 2>;
	auto constexpr c_size = 1000;

	return measure(
		// setup
		[]
		{
			return matrices_type{ {
				create_random_matrix(c_size, c_size),
				create_random_matrix(c_size, c_size),
			} };
		},

		// work
		[](matrices_type const& matrices)
		{
			return matrices[0] * matrices[1];
		}
	);
}
