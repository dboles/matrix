#include "common.hpp"

#include <random>
#include <utility>
#include <vector>

namespace djb::matrix::benchmark {

static auto s_random_device = std::random_device{};
static auto s_mt19937 = std::mt19937{ s_random_device() };
static auto s_uniform_int_distribution = std::uniform_int_distribution<>{0, 100};

auto
get_random_int() -> int
{
	return s_uniform_int_distribution(s_mt19937);
}

auto
create_random_matrix(Matrix<int>::size_type const height,
                     Matrix<int>::size_type const width) -> Matrix<int>
{
	auto elements = std::vector<int>{};
	elements.reserve(height * width);

	for (auto j = elements.capacity(); j--;) {
		elements.emplace_back( get_random_int() );
	}

	return Matrix<int>{ height, width, std::move(elements) };
}

} // namespace djb::matrix::benchmark
