#ifndef DJB_MATRIX_BENCHMARK_COMMON_HPP
#define DJB_MATRIX_BENCHMARK_COMMON_HPP

#include "Matrix.hpp"

#include <chrono>
#include <concepts>
#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <span>
#include <type_traits>

namespace djb::matrix::benchmark {

auto get_random_int() -> int;

auto create_random_matrix(Matrix<int>::size_type const height,
                          Matrix<int>::size_type const width) -> Matrix<int>;

template <std::invocable T_Func_Setup,
          std::invocable< std::invoke_result_t<T_Func_Setup> > T_Func_Work>
auto
measure(T_Func_Setup const& func_setup, T_Func_Work const& func_work) -> int
{
	using Clock = std::chrono::high_resolution_clock;

	auto constexpr iterations = 10;

	auto total_duration = Clock::duration{};
	auto optimisation_defeater = std::byte{0};

	for (auto i = iterations; i--;) {
		auto const matrices = func_setup();

		auto const then = Clock::now();
		auto const result = func_work(matrices);
		auto const now = Clock::now();

		auto const duration = now - then;
		total_duration += duration;

		std::cout << duration.count() << std::endl;

		auto const bytes = std::as_bytes( std::span{&result, 1} );
		auto const index = std::abs( get_random_int() ) % bytes.size();
		optimisation_defeater ^= bytes[index];
	}

	auto const average_duration = total_duration / iterations;
	std::cout << "\naverage:\n" << average_duration.count() << std::endl;

	return std::to_integer<int>(optimisation_defeater);
}

} // namespace djb::matrix::benchmark

#endif // DJB_MATRIX_BENCHMARK_COMMON_HPP
